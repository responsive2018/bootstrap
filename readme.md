# Bootstrap

Referencia [Bootstrap](http://getbootstrap.com/)


Bootstrap es un kit de herramientas de código abierto para desarrollar con HTML, CSS y JS. Hecho para realizar prototipos de ideas ideas o apra poder crear una aplicación completa con nuestras variables y mixins de Sass, sistema de cuadrícula sensible, componentes precompilados extensos y potentes complementos basados en jQuery.

En el presente documento, se dejarán ejemplos y ejercicios de como se implementara bootstrap dentro de nuestras WEB

Originalmente bootstrap:

* Utiliza Flexbox
* Esta a 12 columnas
* Utiliza Sass
* Utiliza jQuery


Ahora con la nueva funcionalidad de Flexbox, tenemos mucho mas dinamismo al momento de maquetar, ya que ahora se manejan las mismas 12 columnas originales como base pero ahora se puede tomar una mira diferente teniendo columnas variables en tamaño o autoajustables.

## ejercicio

  - Hacer un nav con los siguientes titulos
    - Home
    - Quienes somos
    - Testimoniales
    - Nuestros clientes
    - Contacto
  - En el home
    - Tenedremos un slider de entrada
    - Abajo tendremos un video de youtube
    - y texto simulado de la empresa
  - Quienes somos
    - Texto simulado lorem ipsum a 2 columnas responsivas
    - abajo una imagen responsiva de una empresa
  - Testimoniales
    - Será imagen y texto donde:
      - WEB
        - Imagen a la izquierda y texto a la derecha siempre centrado sin importar el numero de lineas
        - texto izquierda imagen derecha
      - Mobile
        - Imagen arriba
        - Texto abajo
  - Nuestros clientes
    - Galeria con thumbnailsy modales
      - Doy clien en la imagen "thumbnail" y me la muestra en grande en una modal
  - Contacto
    - Formulario con:
      - Nombre
      - Calle
      - Estado
      - Mun
      - Cp
      - Telefono
      - Correo
    - validaciones en:
      - Nombre, calle => solo alphas
      - Estado y Mun => selects obligatorios
      - CP => limitar longitud a 5 caracteres y solo numericos
      - Telefono => max y min 10 carécteres solo numericos
      - Correo => validar con expresiones regulares que es tipo correo (jQuery)


### accesos SFTP

    User: sacti
    Pass: Sacti2018.
    Host: curso.contodoweb.com


URL para conectar el formulario
  * http://curso.contodoweb.com/formulario/lectura_formulario.php
  * https://swapi.co/