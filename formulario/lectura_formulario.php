<?php 
	// lectura de informacion
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Max-Age: 1000');
	header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

	// se hace la recepcion de datos y hacemos que se esperen en contestar 5 seg
	if ( count( $_GET ) > 0 ) {
		// sabemos que los datos llegan por GET
		$datos = $_GET;
		$datos["tipoRequest"] = "GET";	
	} else {
		$datos = $_POST;
		$datos["tipoRequest"] = "POST";	
	}
	session_start();

	if ( isset( $_SESSION["counter"] ) ) {
		$counter 				= $_SESSION["counter"];
		$_SESSION["counter"] 	= $counter + 1;

	} else {
		$_SESSION["counter"] = 1;
	}
	$result = $_SESSION["counter"] % 3;
	if ( $result ===  0)  {
		echo json_encode( array("status" => "success", "message" => "Informacion recibida correctamente", "info" => $datos) );
	} else if( $result === 1 ) {
		echo json_encode( array("status" => "warning", "message" => "Faltan datos") );
	} else {
		echo json_encode( array("status" => "error", "message" => "Ocurrió un error en el servidor. Intentelo mas tarde.") );
	}





